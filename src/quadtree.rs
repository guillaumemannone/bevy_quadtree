use std::{sync::Arc, fmt::Debug};

pub trait QuadtreeData {
    fn split_data(&self) -> [Box<Self>; 4];
    fn create_data(center: (f32, f32), length: f32) -> Self;
}

#[derive(Debug)]
pub struct Quadtree<V>
where 
    V: QuadtreeData + Copy
{
    _center: (f32, f32),
    child: Box<Vec<Quadtree<V>>>,
    data: Option<Arc<V>>,
    pub length: f32,
    max_depth: u32
}

impl<V> Quadtree<V> 
where 
    V: QuadtreeData + Copy + Debug
{
    pub fn new(
        center: (f32, f32), 
        length: f32, 
        data: V, 
        max_depth: u32
    ) -> Self 
    {
        Quadtree {
            _center: center,
            length,
            data: Some(Arc::new(data)),
            max_depth,
            child: Box::new(Vec::new()),
        }
    }

    pub fn get_data(&self) -> Arc<V> {
        return self.data.as_ref().unwrap().clone();
    }

    pub fn get_leafs(&self) -> Vec<&Quadtree<V>> {
        if self.child.is_empty() {
            return vec![self];
        }
        else {
            let mut leaves = Vec::new();
            for child in self.child.iter() {
                leaves.append(child.get_leafs().as_mut());
            }
            leaves
        }
    }

    pub fn increase_precision(
        &mut self, 
        point: (f32, f32)
    ) {
        while point.0.abs() > (self._center.0.abs() + self.length) || 
              point.1.abs() > (self._center.1.abs() + self.length) {
            
            if self._center.0 != 0.0 || self._center.1 != 0.0 {
                panic!("should not be able to enlarge inside the quadtree");
            }
            self.enlarge_qt();
        }

        let leaf: &mut Quadtree<V> = self.get_leaf(point);
        if leaf.max_depth <= 0 {
            return;
        } else {
            leaf.split_qt();
            leaf.increase_precision(point);
        }
    }

    fn enlarge_qt(&mut self) {
        self.max_depth += 1;
        self.length *= 2.0;

        if self.child.is_empty() {
            self.split_qt();
        }

        self.data = Some(Arc::new(V::create_data(self._center, self.length)));

        let child_one = self.child.pop().unwrap();
        let child_two = self.child.pop().unwrap();      
        let child_three = self.child.pop().unwrap();
        let child_four = self.child.pop().unwrap();
        
        self.split_qt();
        for child in self.child.as_mut().iter_mut() {
            child.split_qt();
        }

        self.child.get_mut(3).unwrap().child.remove(0);
        self.child.get_mut(3).unwrap().child.insert(0, child_one);

        self.child.get_mut(2).unwrap().child.remove(1);
        self.child.get_mut(2).unwrap().child.insert(1, child_two);

        self.child.get_mut(1).unwrap().child.remove(2);
        self.child.get_mut(1).unwrap().child.insert(2, child_three);

        self.child.get_mut(0).unwrap().child.remove(3);
        self.child.get_mut(0).unwrap().child.insert(3, child_four);

    }

    fn split_qt(&mut self) {
        let new_datas = self.data.as_mut().unwrap().split_data();
        let mut childs: Vec::<Quadtree<V>> = Vec::new();
        for  (index, new_data) in new_datas.iter().enumerate() {
            let child_center = self.get_child_center(index);
            let child_length = self.length.clone() / 2.0;
            

            let child = Quadtree {
                _center: child_center,
                length: child_length,
                data: Some(Arc::new(*new_data.as_ref())),
                max_depth: self.max_depth - 1,
                child: Box::new(Vec::new())
            };

            childs.push(child);
        }

        self.data = None;
        self.child = Box::new(childs);
    }

    fn get_child_center(&self, index: usize) -> (f32, f32) {
        let x: f32;
        let y: f32;
        if index % 2 == 1 {
            x = self._center.0 + self.length / 2.0;
        } else {
            x = self._center.0 - self.length / 2.0;
        }

        if index < 2 {
            y = self._center.1 - self.length / 2.0;
        } else {
            y = self._center.1 + self.length / 2.0;
        }

        return (x, y);
    }

    fn get_leaf(&mut self, point: (f32, f32)) -> &mut Self {
        if self.child.is_empty() {
            self
        }
        else {
            let index = self.get_index(point);
            
            self.child[index].get_leaf(point)
        }
    }

    fn get_index(&self, point: (f32, f32)) -> usize {
        let mut index = 0;
        if self._center.0 < point.0 {index += 1}
        if self._center.1 < point.1 {index += 2}

        index
    }
}
