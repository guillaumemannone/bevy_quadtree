use bevy::prelude::*;
use super::components::{QtData, GameCamera};
use super::resources::QtHolder;


pub fn camera_setup(
    mut commands: Commands
) {
    commands.spawn(
        (Camera2dBundle {
        transform: Transform::from_xyz(0.0, 0.0, 0.0),
        ..default()
        },
        GameCamera)
    );
}

pub fn move_camera(
    keyboard_input: Res<Input<ScanCode>>,
    mut camera_query: Query<&mut Transform, With<GameCamera>>,
    time: Res<Time>,
) {
    if let Ok(mut transform) = camera_query.get_single_mut() {
        let mut direction = Vec3::ZERO;

        if keyboard_input.pressed(ScanCode(77)) /* RIGHT key */ || keyboard_input.pressed(ScanCode(32) /* D key */) {
            direction += Vec3::new(1.0, 0.0, 0.0);
        }
        if keyboard_input.pressed(ScanCode(75)) /* LEFT key */ || keyboard_input.pressed(ScanCode(30) /* A key */) {
            direction += Vec3::new(-1.0, 0.0, 0.0);
        }

        if keyboard_input.pressed(ScanCode(72)) /* UP key */ || keyboard_input.pressed(ScanCode(17) /* W key */) {
            direction += Vec3::new(0.0, 1.0, 0.0);
        }
        if keyboard_input.pressed(ScanCode(80)) /* DOWN key */ || keyboard_input.pressed(ScanCode(31) /* S key */) {
            direction += Vec3::new(0.0, -1.0, 0.0);
        }

        if keyboard_input.pressed(ScanCode(2)) {
            transform.scale *= 1.01
        }
        if keyboard_input.pressed(ScanCode(3)) {
            transform.scale *= 0.99
        }

        transform.translation += direction * 300.0 * time.delta_seconds();
    }
}

pub fn on_camera_moved(
    mut camera_query: Query<&mut Transform, With<GameCamera>>,
    mut quadtree: ResMut<QtHolder>,

) {
    if let Ok(camera_transform) = camera_query.get_single_mut() {
        let cam_position = camera_transform.translation;        
        quadtree.quadtree.increase_precision((cam_position.y, -cam_position.x));
    }
}

pub fn spawn_shapes(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    leaf_query: Query<Entity, With<QtData>>,
    quadtree: ResMut<QtHolder>
) {

    for old_leaves in leaf_query.iter() {
        commands.entity(old_leaves).despawn();
    }

    let leaves = quadtree.quadtree.get_leafs();
    for leaf in leaves.iter().map(|f| f.get_data()) {
        commands.spawn((
            SpriteBundle {
                transform: Transform::from_xyz(leaf.center.0, leaf.center.1, 0.0)
                    .with_scale(Vec3 { x: leaf.length / 8.0, y: leaf.length / 8.0, z: 0.0 }),
                texture: asset_server.load("square.png"),
                ..default()
            },
            QtData {center: leaf.center, length: leaf.length}
        ));
    }
}

