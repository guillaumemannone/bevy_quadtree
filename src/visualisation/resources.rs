use bevy::prelude::*;
use super::Quadtree;
use super::components::QtData;

#[derive(Resource)]
pub struct QtHolder {
    pub quadtree: Quadtree<QtData>
}

impl Default for QtHolder {
    fn default() -> Self {
        QtHolder { 
            quadtree: Quadtree::new(
            (0.0, 0.0), 
            8.0, 
            QtData { length: 8.0, center: (0.0, 0.0) }, 
            1
        )}
    }
}
