use crate::quadtree::Quadtree;
use super::quadtree::QuadtreeData;
use bevy::prelude::*;


pub mod components;
mod resources;
mod systems;

use resources::*;
use systems::*;


pub struct VisualisationPlugin;

impl Plugin for VisualisationPlugin {
    fn build(&self, app: &mut App) {
            app .init_resource::<QtHolder>()
                .add_systems(Update, (move_camera, on_camera_moved, spawn_shapes))
                .add_systems(Startup, (camera_setup, spawn_shapes));
    }
}