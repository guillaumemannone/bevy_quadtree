use bevy::prelude::*;
use super::QuadtreeData;

#[derive(Component)]
pub struct GameCamera;

#[derive(Component, Default, Debug, Clone, Copy)]
pub struct QtData  {
    pub length: f32,
    pub center: (f32, f32)
}

impl QuadtreeData for QtData {
    fn split_data(&self) -> [Box<QtData>; 4] {
        return [
            Box::new(Self::create_data(
                (self.center.0 + self.length / 2., self.center.1 - self.length / 2.),
                self.length / 2.
            )),
            Box::new(Self::create_data(
                (self.center.0 + self.length / 2., self.center.1 + self.length / 2.),
                self.length / 2.
            )), 
            Box::new(Self::create_data(
                (self.center.0 - self.length / 2., self.center.1 - self.length / 2.),
                self.length / 2.
            )), 
            Box::new(Self::create_data(
                (self.center.0 - self.length / 2., self.center.1 + self.length / 2.),
                self.length / 2.
            ))
            ];
    }

    fn create_data(center: (f32, f32), length: f32) -> Self {

        let data = QtData { 
            center,
            length
        };
        
        data 
    }
}
