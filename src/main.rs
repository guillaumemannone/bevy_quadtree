use bevy::prelude::*;
use visualisation::VisualisationPlugin;

mod quadtree;
mod visualisation;


fn main() {
    App::new()
        .add_plugins((DefaultPlugins, VisualisationPlugin))
        .run();
}
